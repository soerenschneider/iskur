import unittest
from unittest import TestCase

import pendulum
from iskur.data import Hourly
from iskur.segments import Segment, Segments

class Test_Data(TestCase):

    def test_find_extremes(self):
        datapoints = list()
        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 10, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=-15, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))
        
        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 11, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=35, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=3, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))
        
        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 12, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 13, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=-24, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 14, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 15, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=8, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 16, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 17, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 18, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 19, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 20, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 21, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 22, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 3, 31, 23, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 4, 1, 0, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        datapoints.append(Hourly(timestamp=pendulum.datetime(2013, 4, 1, 1, 59, 59), \
                summary="Bla", \
                city="City", \
                temp=30, \
                temp_apparent=4, \
                precip_intensity=0.1, \
                precip_prob=0.5, \
                precip_type="rain", \
                wind_speed=8, \
                wind_gust=9, \
                uv_index=1, \
                ozone=1, \
                cloudiness=0.23, \
                humidity=0.50, \
                visibility=13, \
                pressure=123))

        segment = Segments().get_segments(datapoints)
        self.assertEqual(1, len(segment))
        self.assertEqual(segment[0].uv_min, 1)
        self.assertEqual(segment[0].uv_max, 8)
        self.assertEqual(segment[0].temp_min, -24)
        self.assertEqual(segment[0].temp_max, 30)

if __name__ == '__main__':
    unittest.main()
