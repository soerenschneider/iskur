import os
import argparse
import logging
import unittest
import pendulum
from unittest import TestCase

import psycopg2
from iskur.providers import WeatherbitProvider
from iskur.postgres import PostgresPersistence
from iskur.app import Iskur

class Test_Systemtest(TestCase):
    host=os.getenv("POSTGRES_HOST", "localhost")
    user=os.getenv("POSTGRES_DB", "iskur")
    pw=os.getenv("POSTGRES_USER", "iskur")
    db=os.getenv("POSTGRES_PASSWORD", "iskur")
    mountebank_host=os.getenv("MOUNTEBANK_HOST", "localhost")

    @staticmethod
    def setUpClass():
        logging.basicConfig(level=logging.DEBUG, format="%(levelname)s\t %(asctime)s %(message)s")

        db = psycopg2.connect(host=Test_Systemtest.host, user=Test_Systemtest.user, password=Test_Systemtest.pw, dbname=Test_Systemtest.db)
        with db.cursor() as cursor:
            with open('postgres/01-init.sql','r') as sql_file:
                cursor.execute(sql_file.read())
        db.commit()
        db.close()

    def setUp(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor() as cursor:
            sql = "DELETE FROM daily; DELETE FROM hourly; DELETE FROM wearther; DELETE FROM alerts; "
            cursor.execute(sql)
        db.commit()
        db.close()

    def test_systemtest(self):
        args = argparse.Namespace(pg_host=self.host, pg_user=self.user, pg_pw=self.pw)
        db = PostgresPersistence(args)
        provider_config = dict()
        provider_config["api_key"] = "bla"
        provider_config["provider_name"] = "weatherbit"
        provider_config["lat"] = "5"
        provider_config["lon"] = "6"
        provider_config["nice_name"] = "Niceee"
        provider_config["url"] = "http://" + self.mountebank_host + ":8080"
        provider = WeatherbitProvider(provider_config)
        impl = Iskur(args, [provider], db)
        impl.once()


        self.assertEqual(len(self.read_alerts()), 0)
        self.assertEqual(len(self.read_daily()), 16)
        self.assertEqual(len(self.read_hourly()), 48)
        self.assertEqual(len(self.read_wearther()), 5)

    def read_wearther(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor() as cursor:
            sql = f"SELECT * FROM wearther"
            cursor.execute(sql)
            return cursor.fetchall()

    def read_daily(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor() as cursor:
            sql = f"SELECT * FROM daily"
            cursor.execute(sql)
            return cursor.fetchall()

    def read_hourly(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor() as cursor:
            sql = f"SELECT * FROM hourly"
            cursor.execute(sql)
            return cursor.fetchall()
    
    def read_alerts(self):
        db = psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.db)
        with db.cursor() as cursor:
            sql = f"SELECT * FROM alerts"
            cursor.execute(sql)
            return cursor.fetchall()