import json

from iskur.providers import WeatherbitProvider, OpenWeatherProvider


class ProviderConfig:
    @staticmethod
    def read_config(path):
        if not path:
            raise ValueError("No path given")

        with open(path, "r") as content:
            return json.load(content)

        raise ValueError("Could not load config")

    @staticmethod
    def check_config(config):
        if not config:
            raise ValueError("Empty config")

        if "providers" not in config:
            raise ValueError("Missing providers")

        for provider in config["providers"]:
            if "provider_name" not in provider:
                raise ValueError("Missing 'provider_name' in config")

            provider_name = provider["provider_name"]
            for element in ["api_key", "lat", "lon", "nice_name"]:
                if element not in provider:
                    raise ValueError(f"Missing '{element}' in provider {provider_name}")

    @staticmethod
    def get_providers(path):
        config = ProviderConfig.read_config(path)
        ProviderConfig.check_config(config)

        providers = list()
        for provider in config["providers"]:
            providers.append(ProviderConfig.get_provider(provider))

        return providers

    @staticmethod
    def get_provider(provider):
        if provider["provider_name"] == WeatherbitProvider.NAME:
            return WeatherbitProvider(provider)

        if provider["provider_name"] == OpenWeatherProvider.NAME:
            return OpenWeatherProvider(provider)

        raise ValueError(f"Unknown provider: {provider['provider_name']}")
