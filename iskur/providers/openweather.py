import pendulum

from iskur.data import Hourly, Daily, Alert
from iskur.providers.provider import AbstractProvider

# TODO: Incomplete
class OpenWeatherProvider(AbstractProvider):
    """ Provider for openweathermap.org """

    NAME = "openweather"

    def __init__(self, config):
        self.city = config["nice_name"]

        url = "https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude=minutely&appid={key}&units=metric"
        if "url" in config:
            url = config["url"]

        self.api_endpoint = url.format(
            key=config["api_key"], lat=config["lat"], lon=config["lon"]
        )

    def get_forecast(self):
        hourly = self.query(self.api_endpoint)

        return self.get_hourly(hourly), [], []

    def parse_reply(self, response: str):
        return self.get_hourly(response)

    def get_hourly(self, weather):
        """ Converts API response to data object for hourly forecast data. """
        hourly = list()
        for i in weather["hourly"]:
            entry = self.build_hourly(i)
            hourly.append(entry)

        return hourly

    def build_hourly(self, entry) -> Hourly:
        """ Converts a single row of hourly forecast data to a data object. """
        if not entry:
            raise ValueError("No entry given")

        return Hourly(
            timestamp=pendulum.from_timestamp(entry["dt"]),
            city=self.city,
            summary=entry["weather"][0]["description"],
            temp=entry["temp"],
            temp_apparent=entry["feels_like"],
            precip_prob=entry["pop"],
            wind_speed=entry["wind_speed"],
            cloudiness=entry["clouds"],
            humidity=entry["humidity"],
            visibility=entry["visibility"],
            pressure=entry["pressure"],
            icon=entry["weather"][0]["icon"],
        )

    def get_daily(self, weather):
        """ Converts API response to data object for daily forecast data. """
        daily = list()
        for i in weather["daily"]["data"]:
            entry = self.build_daily(i)
            daily.append(entry)

        return daily

    def build_daily(self, entry) -> Daily:
        """ Converts a single row of daily forecast data to a data object. """
        if not entry:
            raise ValueError("No entry given")

        return Daily(
            timestamp=pendulum.from_timestamp(entry["time"]),
            city=self.city,
            summary=entry["summary"],
            precip_intensity=entry["precipIntensity"],
            precip_prob=entry["precipProbability"],
            precip_type=entry.get("precipType"),
            wind_speed=entry["windSpeed"],
            wind_gust=entry["windGust"],
            uv_index=entry["uvIndex"],
            uv_index_time=pendulum.from_timestamp(entry["uvIndexTime"]),
            ozone=entry["ozone"],
            cloudiness=entry["cloudCover"],
            humidity=entry["humidity"],
            visibility=entry["visibility"],
            pressure=entry["pressure"],
            sunrise=pendulum.from_timestamp(entry["sunriseTime"]),
            sunset=pendulum.from_timestamp(entry["sunsetTime"]),
            moonphase=entry["moonPhase"],
            temp_high=entry["temperatureHigh"],
            temp_high_time=pendulum.from_timestamp(entry["temperatureHighTime"]),
            temp_low=entry["temperatureLow"],
            temp_low_time=pendulum.from_timestamp(entry["temperatureLowTime"]),
            temp_app_high=entry["apparentTemperatureHigh"],
            temp_app_high_time=pendulum.from_timestamp(
                entry["apparentTemperatureHighTime"]
            ),
            temp_app_low=entry["apparentTemperatureLow"],
            temp_app_low_time=pendulum.from_timestamp(
                entry["apparentTemperatureLowTime"]
            ),
            precip_intensity_max=entry["precipIntensityMax"],
            precip_intensity_max_time=pendulum.from_timestamp(
                entry.get("precipIntensityMaxTime")
            ),
            wind_gust_time=pendulum.from_timestamp(entry["windGustTime"]),
            icon=entry["icon"],
        )
