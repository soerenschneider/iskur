from urllib.parse import urljoin

import pendulum

from iskur.data import Hourly, Daily, Alert
from iskur.providers.provider import AbstractProvider


class WeatherbitProvider(AbstractProvider):
    """ Data provider for weatherbit.io """

    NAME = "weatherbit"

    def __init__(self, config):
        self.city = config["nice_name"]

        url = "https://api.weatherbit.io"
        if "url" in config:
            url = config["url"]

        self.endpoint_hourly = urljoin(
            url,
            f"v2.0/forecast/hourly?key={config['api_key']}&lat={config['lat']}&lon={config['lon']}&hours=120",
        )
        self.endpoint_daily = urljoin(
            url,
            f"v2.0/forecast/daily?key={config['api_key']}&lat={config['lat']}&lon={config['lon']}",
        )

    def get_forecast(self):
        """ Triggers calling all endpoints. """
        hourly = None
        try:
            hourly = self.query(self.endpoint_hourly)
        except:
            # TODO: add metrics, add logging
            pass

        daily = None
        try:
            daily = self.query(self.endpoint_daily)
        except:
            # TODO: add metrics, add logging
            pass

        return (
            self._get_hourly(hourly),
            self._get_daily(daily),
            [],
        )

    def _get_hourly(self, weather):
        """ Converts API response to data object for hourly forecast data. """
        if not weather or "data" not in weather:
            return []
 
        hourly = list()
        for i in weather["data"]:
            entry = self._build_hourly(i)
            hourly.append(entry)

        return hourly

    def _build_hourly(self, entry) -> Hourly:
        """ Converts a single row of hourly forecast data to a data object. """
        if not entry:
            raise ValueError("No entry given")
        return Hourly(
            timestamp=pendulum.from_timestamp(entry["ts"]),
            city=self.city,
            summary=entry["weather"]["description"],
            temp=entry["temp"],
            temp_apparent=entry["app_temp"],
            precip_prob=entry["pop"],
            wind_speed=entry["wind_spd"],
            wind_gust=entry["wind_gust_spd"],
            cloudiness=entry["clouds"],
            humidity=entry["rh"],
            visibility=entry["vis"],
            precip_type="rain" if entry["snow"] < 0.1 else "snow",
            ozone=entry["ozone"],
            precip_intensity=entry["precip"],
            uv_index=entry["uv"],
            pressure=entry["pres"],
            icon=entry["weather"]["icon"],
        )

    def _get_daily(self, weather):
        """ Converts API response to data object for daily forecast data. """
        if not weather or "data" not in weather:
            return []

        daily = list()
        for i in weather["data"]:
            entry = self._build_daily(i)
            daily.append(entry)

        return daily

    def _build_daily(self, entry) -> Daily:
        """ Converts a single row of daily forecast data to a data object. """
        if not entry:
            raise ValueError("No entry given")

        return Daily(
            timestamp=pendulum.from_timestamp(entry["ts"]),
            city=self.city,
            summary=entry["weather"]["description"],
            precip_intensity=entry["precip"],
            precip_prob=entry["pop"],
            wind_speed=entry["wind_spd"],
            wind_gust=entry["wind_gust_spd"],
            uv_index=entry["uv"],
            ozone=entry["ozone"],
            cloudiness=entry["clouds"],
            humidity=entry["rh"],
            visibility=entry["vis"],
            pressure=entry["pres"],
            sunrise=pendulum.from_timestamp(entry["sunrise_ts"]),
            sunset=pendulum.from_timestamp(entry["sunset_ts"]),
            moonphase=entry["moon_phase"],
            temp_high=entry["max_temp"],
            temp_low=entry["min_temp"],
            temp_app_high=entry["app_max_temp"],
            temp_app_low=entry["app_min_temp"],
            icon=entry["weather"]["icon"],
            precip_type="rain" if entry["snow"] < 0.1 else "snow",
        )
