import backoff
import requests


class AbstractProvider:
    NAME = "AbstractProvider"

    @backoff.on_exception(
        backoff.expo,
        (
            requests.exceptions.Timeout,
            requests.exceptions.ConnectionError,
            requests.exceptions.RequestException,
        ),
        max_time=60,
    )
    def query(self, endpoint):
        response = requests.get(endpoint)
        if not response.ok:
            if response.status_code in [401, 403]:
                raise Exception("Unauthorized")

            if response.status_code >= 500:
                raise Exception("API Error")

            raise Exception(f"Unknown error, status code {response.status_code}")

        return response.json()

    def __repr__(self):
        return self.NAME
