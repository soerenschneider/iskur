from dataclasses import dataclass
import pendulum


@dataclass
class Alert:
    """ Data object representation of an alert. """

    starts: pendulum
    expires: pendulum
    title: str
    description: str


@dataclass
class Hourly:
    """ Data object representation of an hourly weather forecast. """

    timestamp: pendulum
    city: str
    summary: str
    temp: float
    temp_apparent: float
    precip_intensity: float
    precip_prob: float
    precip_type: str
    wind_speed: float
    wind_gust: float
    uv_index: int
    ozone: float
    cloudiness: float
    humidity: float
    visibility: float
    pressure: float
    icon: str = ""


@dataclass
class Daily:
    """ Data object representation of a daily weather forecast. """

    timestamp: pendulum
    city: str
    summary: str
    precip_intensity: float
    precip_prob: float
    precip_type: str
    wind_speed: float
    wind_gust: float
    uv_index: int
    ozone: float
    cloudiness: float
    humidity: float
    visibility: float
    pressure: float
    sunrise: pendulum
    sunset: pendulum
    moonphase: float
    temp_high: float
    temp_low: float
    temp_app_high: float
    temp_app_low: float
    icon: str = ""
