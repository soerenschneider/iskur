from iskur.data import Hourly
from iskur.segments import Segment
from iskur.wear.wear import Wear
from iskur.wear.constants import UV_VERY_HIGH


class Top(Wear):
    pass


class HeavySweater(Top):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp < 10

    def weight(self) -> int:
        return 1


class Sweater(Top):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp >= 10

    def weight(self) -> int:
        return 2


class Shirt(Top):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp >= 20

    def weight(self) -> int:
        return 3


class TeeShirt(Top):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp >= 25 and weather_data.uv_index < UV_VERY_HIGH

    def weight(self) -> int:
        return 4
