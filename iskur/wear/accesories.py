from iskur.data import Hourly
from iskur.wear.wear import Wear
from iskur.wear.constants import UV_LOW, UV_SEMI, UV_HIGH, UV_VERY_HIGH, STORM


class Accessory(Wear):
    pass


class Umbrella(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.precip_prob >= 0.75 or weather_data.precip_intensity >= 1

    def weight(self) -> int:
        return 0


class Beanie(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return (
            weather_data.uv_index < UV_HIGH
            and weather_data.temp <= 7
            or (weather_data.wind_speed >= STORM or weather_data.wind_gust >= STORM)
        )

    def weight(self) -> int:
        return 0


class Cap(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return (
            weather_data.uv_index > UV_LOW
            and weather_data.wind_speed <= STORM
            and weather_data.wind_gust <= STORM
        )

    def weight(self) -> int:
        return 0


class SunHat(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.uv_index >= UV_HIGH

    def weight(self) -> int:
        return 0


class Scarf(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return (
            weather_data.wind_gust >= STORM or weather_data.wind_speed >= STORM
        ) or weather_data.temp <= 6

    def weight(self) -> int:
        return 0


class Gloves(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp <= 3

    def weight(self) -> int:
        return 0


class Shades(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.uv_index >= UV_LOW or weather_data.cloudiness <= 0.2

    def weight(self) -> int:
        return 0


class SunLotion(Accessory):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.uv_index >= UV_LOW

    def property(self, weather_data: Hourly) -> list:
        if weather_data.uv_index >= UV_VERY_HIGH:
            return ["50+"]
        if weather_data.uv_index >= UV_HIGH:
            return ["30-50"]
        return ["15-50"]

    def weight(self) -> int:
        return 0
