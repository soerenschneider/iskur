import re

from iskur.data import Hourly
from iskur.segments import Segment
from iskur.wear.wear import Wear
from iskur.wear.constants import UV_HIGH, STORM


class Pants(Wear):
    pass


class Trousers(Pants):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp <= 26

    def property(self, weather_data: Hourly) -> list:
        ret = []
        if weather_data.temp < 5:
            ret.append("heavy")
        if weather_data.uv_index >= UV_HIGH:
            ret.append("uv repellent")
        if weather_data.precip_instensity >= 1:
            ret.append("water repellent")
        if weather_data.wind_speed >= STORM or weather_data.wind_gust >= STORM:
            ret.append("windbreaking")
        return ret

    def weight(self) -> int:
        return 2


class Shorts(Pants):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp > 26 and weather_data.uv_index <= UV_HIGH

    def weight(self) -> int:
        return 4
