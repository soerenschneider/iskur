import re

from iskur.data import Hourly
from iskur.segments import Segment
from iskur.wear.jackets import Jacket
from iskur.wear.top import Top
from iskur.wear.pants import Pants
from iskur.wear.shoes import Shoes
from iskur.wear.accesories import Accessory


class Outfit:
    @property
    def jacket(self):
        return self._jacket

    @jacket.setter
    def jacket(self, value):
        self._jacket = value

    @property
    def top(self):
        return self._top

    @top.setter
    def top(self, value):
        self._top = value

    @property
    def shoes(self):
        return self._shoes

    @shoes.setter
    def shoes(self, value):
        self._shoes = value

    @property
    def pants(self):
        return self._pants

    @pants.setter
    def pants(self, value):
        self._pants = value

    @property
    def accessories(self):
        return self._accessories

    @accessories.setter
    def accessories(self, value):
        self._accessories = value

    def accessories_formatted(self):
        return ", ".join(self.accessories).strip()

    def __repr__(self):
        return f"{self.jacket}, {self.top}, {self.pants}, {self.shoes}"


class Wear:
    def is_appropriate(self, weather_data: Hourly) -> bool:
        raise Exception("Not implemented")

    def __repr__(self):
        name = self.__class__.__name__
        split = re.sub(
            "([A-Z][a-z]+)", r" \1", re.sub("([A-Z]+)", r" \1", name)
        ).split()
        return " ".join(split)

    def weight(self) -> int:
        raise Exception("Not implemented")


class WearAnalyzer:
    @staticmethod
    def getSubclasses(cls) -> list:
        all_subclasses = list()

        for subclass in cls.__subclasses__():
            all_subclasses.append(subclass)
            all_subclasses.extend(WearAnalyzer.getSubclasses(subclass))

        return all_subclasses

    @staticmethod
    def analyze_segment(segment: Segment) -> Wear:
        outfit = WearAnalyzer.analyze(segment.data_points)
        segment.accessories = outfit.accessories_formatted()
        segment.outfit = str(outfit)

    @staticmethod
    def analyze(datapoints: list) -> Wear:
        outfit = Outfit()
        outfit.top = WearAnalyzer.get_top(datapoints)
        outfit.shoes = WearAnalyzer.get_shoes(datapoints)
        outfit.pants = WearAnalyzer.get_pants(datapoints)
        outfit.jacket = WearAnalyzer.get_jacket(datapoints)
        outfit.accessories = WearAnalyzer.get_accessories(datapoints)
        return outfit

    @staticmethod
    def get_shoes(datapoints: list):
        return WearAnalyzer._get_single(datapoints, Shoes)

    @staticmethod
    def get_pants(datapoints: list):
        return WearAnalyzer._get_single(datapoints, Pants)

    @staticmethod
    def get_top(datapoints: list):
        return WearAnalyzer._get_single(datapoints, Top)

    @staticmethod
    def get_jacket(datapoints: list):
        return WearAnalyzer._get_single(datapoints, Jacket)

    @staticmethod
    def _get_single(datapoints: list, cls):
        # get all instances of all subclasses that implement cls
        avail = list(map(lambda x: x(), WearAnalyzer.getSubclasses(cls)))

        appropriate = list()
        for datapoint in datapoints:
            for instance in avail:
                if instance.is_appropriate(datapoint):
                    appropriate.append(instance)

        if not appropriate:
            return None

        return min(appropriate, key=lambda x: x.weight())

    @staticmethod
    def get_accessories(datapoints: list):
        avail_accessories = map(lambda x: x(), WearAnalyzer.getSubclasses(Accessory))

        acc = set()
        for datapoint in datapoints:
            for instance in avail_accessories:
                if instance.is_appropriate(datapoint):
                    acc.add(str(instance))

        return sorted(list(acc))
