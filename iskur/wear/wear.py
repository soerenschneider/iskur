import re

from iskur.data import Hourly


class Wear:
    def is_appropriate(self, weather_data: Hourly) -> bool:
        raise Exception("Not implemented")

    def __repr__(self):
        name = self.__class__.__name__
        split = re.sub(
            "([A-Z][a-z]+)", r" \1", re.sub("([A-Z]+)", r" \1", name)
        ).split()
        return " ".join(split)

    def property(self) -> list:
        return []

    def weight(self) -> int:
        raise Exception("Not implemented")
