from iskur.data import Hourly
from iskur.wear.wear import Wear
from iskur.wear.constants import STORM, UV_HIGH


class Jacket(Wear):
    def property(self, weather_data: Hourly) -> list:
        ret = list()
        if weather_data.precip_intensity >= 1:
            ret.append("waterproof")
        if weather_data.wind_gust >= STORM or weather_data.wind_speed >= STORM:
            ret.append("windbreaking")
        if weather_data.uv_index >= UV_HIGH:
            ret.append("uv-resistant")

        return ret


class HeavyCoat(Jacket):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp < 5

    def weight(self) -> int:
        return 1


class Coat(Jacket):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp < 10

    def weight(self) -> int:
        return 2


class LightJacket(Jacket):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp < 20

    def weight(self) -> int:
        return 3
