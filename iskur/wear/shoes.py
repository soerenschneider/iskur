from iskur.data import Hourly
from iskur.wear.wear import Wear


class Shoes(Wear):
    pass


class Boots(Shoes):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp < 10

    def property(self, weather_data: Hourly) -> list:
        ret = []
        if weather_data.temp < 0:
            ret.append("insulated")
        if weather_data.precip_instensity >= 1:
            ret.append("water-proof")

    def weight(self) -> int:
        return 1


class Sneakers(Shoes):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp >= 10

    def property(self, weather_data: Hourly) -> list:
        ret = []
        if weather_data.temp >= 20:
            ret.append("insulated")
        if weather_data.precip_instensity >= 1:
            ret.append("water repellent")
        return ret

    def weight(self) -> int:
        return 2


class Sandals(Shoes):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp >= 28

    def weight(self) -> int:
        return 3


class FlipFlops(Shoes):
    def is_appropriate(self, weather_data: Hourly) -> bool:
        return weather_data.temp >= 28 and weather_data.precip_intensity >= 1

    def weight(self) -> int:
        return 4
