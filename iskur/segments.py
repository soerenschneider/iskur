from dataclasses import dataclass
import pendulum

SEGMENTS = [4, 12, 20]


@dataclass
class Segment:
    start: pendulum
    end: pendulum
    city: str
    data_points: list

    temp_min: float = 0
    temp_max: float = 0

    precip_min: float = 0
    precip_max: float = 0

    wind_min: float = 0
    wind_max: float = 0

    uv_min: int = 0
    uv_max: int = 0

    outfit: str = ""
    accessories: str = ""

    def __repr__(self):
        return f"{self.start} - {self.end}, uv_min={self.uv_min}, uv_max={self.uv_max}"

    @staticmethod
    def _find_extremes(segment):
        # this is far away from being as elegant as working with multiple lambdas to determine all max/min values
        # but we save a lot of iterations
        temp_min = 1000
        temp_max = -1000
        precip_min = 1000
        precip_max = -1000
        uv_min = 1000
        uv_max = -1000
        wind_min = 1000
        wind_max = -1000

        for data in segment.data_points:
            # it seems counterintuitive to not use an 'elif' here, but if there's only a single datapoint in the list
            # the 'elif' would prevent the second statement to be set
            if data.precip_intensity < precip_min:
                segment.precip_min = data.precip_intensity
                precip_min = data.precip_intensity
            if data.precip_intensity > precip_max:
                segment.precip_max = data.precip_intensity
                precip_max = data.precip_intensity

            if data.temp < temp_min:
                segment.temp_min = data.temp
                temp_min = data.temp
            if data.temp > temp_max:
                segment.temp_max = data.temp
                temp_max = data.temp

            if data.wind_speed < wind_min:
                segment.wind_min = data.wind_speed
                wind_min = data.wind_speed
            if data.wind_speed > wind_max:
                segment.wind_max = data.wind_speed
                wind_max = data.wind_speed

            if data.uv_index < uv_min:
                segment.uv_min = data.uv_index
                uv_min = data.uv_index
            if data.uv_index > uv_max:
                segment.uv_max = data.uv_index
                uv_max = data.uv_index

    @staticmethod
    def build_segment(hourlies: list):
        if not hourlies:
            raise ValueError("No data supplied")

        start = hourlies[0].timestamp
        end = hourlies[len(hourlies) - 1].timestamp

        segment = Segment(
            start=start, end=end, city=hourlies[0].city, data_points=hourlies
        )
        Segment._find_extremes(segment)
        return segment


class Segments:
    def __init__(self, segments=None):
        if not segments:
            segments = SEGMENTS
        self._segments = SEGMENTS

    def is_new_segment(self, hour):
        return hour in self._segments

    @staticmethod
    def sort_hourlies(hourlies: list):
        sorted(hourlies, key=lambda x: x.timestamp)

    def get_segments(self, data_points: list) -> list:
        Segments.sort_hourlies(data_points)
        segments = list()

        segment = list()
        for data in data_points:
            segment.append(data)
            if self.is_new_segment(data.timestamp.hour):
                if self.is_complete_segment(segment):
                    s = Segment.build_segment(segment)
                    segments.append(s)
                segment = list()
                segment.append(data)

        return segments

    def is_complete_segment(self, segment: list) -> bool:
        return len(segment) == 1 + 24 / len(self._segments)
