import logging

from apscheduler.schedulers.blocking import BlockingScheduler

from iskur.postgres import PostgresPersistence
from iskur.metrics import (
    PROM_API_CALLS,
    PROM_API_CALL_TIMESTAMP,
    PROM_API_CALLS_ERRORS,
    PROM_PROCESSED_RECORDS,
    PROM_DATABASE_ERRORS,
)
from iskur.segments import Segments
from iskur.wear.outfit import WearAnalyzer


class Iskur:
    """
    The central place where all data is fetched, processed and persisted.
    """

    def __init__(self, args, providers, database=None):
        if not args:
            raise ValueError("Empty args supplied")

        self.args = args

        if not providers:
            raise ValueError("No valid provider(s) supplied")
        self.providers = providers

        if not database:
            database = PostgresPersistence(args)
        self._database = database

        self.segment_builder = Segments()

    def _query_provider(self, provider) -> None:
        """ Queries the provider and persists the result to the database. """

        logging.info(
            "Fetching weather data from provider %s for location %s",
            provider,
            provider.city,
        )
        hourly, daily, alerts = None, None, None
        try:
            PROM_API_CALLS.inc()
            hourly, daily, alerts = provider.get_forecast()
            logging.info(
                "Fetched %d hourly entries and %d daily entries",
                len(hourly),
                len(daily),
            )
            PROM_API_CALL_TIMESTAMP.labels("success").set_to_current_time()
        except KeyError as err:
            PROM_API_CALLS_ERRORS.inc()
            PROM_API_CALL_TIMESTAMP.labels("error").set_to_current_time()
            logging.error("Received keyerror while fetching weather: %s", err)
            return

        try:
            self._database.persist_hourly(hourly)
            PROM_PROCESSED_RECORDS.labels("hourly").inc()
        except Exception as error:
            PROM_DATABASE_ERRORS.labels("hourly").inc()
            logging.error("Error while persisting hourly records: %s", error)

        try:
            self._database.persist_daily(daily)
            PROM_PROCESSED_RECORDS.labels("daily").inc()
        except Exception as error:
            PROM_DATABASE_ERRORS.labels("daily").inc()
            logging.error("Error while persisting daily records: %s", error)

        try:
            self._database.persist_alerts(alerts)
            PROM_PROCESSED_RECORDS.labels("alerts").inc()
        except Exception as error:
            PROM_DATABASE_ERRORS.labels("alerts").inc()
            logging.error("Error while persisting alerts: %s", error)

        try:
            segments = self.segment_builder.get_segments(hourly)
            for segment in segments:
                WearAnalyzer.analyze_segment(segment)
            self._database.persist_segments(segments)
        except KeyError as error:
            logging.error("Error while persisting segment records: %s", error)

    def once(self):
        """ Fetches the data exactly once from the configured providers. """
        for provider in self.providers:
            self._query_provider(provider)

    def consume(self):
        """ Continuously fetches data from the configured providers in the configured interval. """
        self.once()
        scheduler = BlockingScheduler()
        scheduler.add_job(self.once, "interval", seconds=self.args.sleep)
        scheduler.start()
