import sys
import logging

import configargparse
from prometheus_client import start_http_server

from iskur.providers.builder import ProviderConfig
from iskur.app import Iskur


def init():
    """ Bootstraps all needed information and starts the application. """
    args = parse_args()
    setup_logging(args.debug)
    print_config(args)
    setup_prometheus(args.prom_port)

    try:
        providers = ProviderConfig.get_providers(args.config_file)
    except ValueError as err:
        logging.error("Can not build providers: %s", err)
        sys.exit(1)

    iskur = Iskur(args, providers)
    iskur.consume()


def parse_args() -> configargparse.Namespace:
    """ Parses the configuration and returns the Namespace object. """
    parser = configargparse.ArgumentParser(prog="iskur")

    parser.add_argument(
        "--pg-host",
        dest="pg_host",
        action="store",
        env_var="ISKUR_PG_HOST",
        default="localhost",
    )
    parser.add_argument(
        "--pg-user",
        dest="pg_user",
        action="store",
        env_var="ISKUR_PG_USER",
        default="iskur",
    )
    parser.add_argument(
        "--pg-pw", dest="pg_pw", action="store", env_var="ISKUR_PG_PW", default="iskur"
    )
    parser.add_argument(
        "--sleep",
        dest="sleep",
        action="store",
        env_var="ISKUR_SLEEP",
        type=int,
        default=3600,
    )
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        env_var="ISKUR_DEBUG",
        default=False,
    )

    parser.add_argument(
        "--prom-port",
        dest="prom_port",
        action="store",
        env_var="ISKUR_PROM_PROT",
        type=int,
        default=9190,
    )

    parser.add_argument(
        "--config",
        dest="config_file",
        action="store",
        env_var="ISKUR_CONFIG",
        required=True,
    )

    return parser.parse_args()


def print_config(args) -> None:
    """
    Helps the user verifying the correct configuration of the app.
    Logs all parsed configuration options.
    """
    logging.info("Started iskur using the following arguments:")
    logging.info("config=%s", args.config_file)
    logging.info("pg_host=%s", args.pg_host)
    logging.info("pg_user=%s", args.pg_user)
    logging.info("interval=%d", args.sleep)
    logging.info("debug=%s", args.debug)
    logging.info("prom-port=%s", args.prom_port)


def setup_prometheus(port=None) -> None:
    """ Starts the prometheus http server on the given port. """
    if not port or not 0 < port < 65535:
        logging.info("Not starting prometheus http server")
        return

    logging.info("Starting prometheus http server on port %s", port)
    start_http_server(port)
    logging.info("Successfully set up prometheus server!")


def setup_logging(debug=False):
    """ Sets up the logging. """
    loglevel = logging.INFO
    if debug:
        loglevel = logging.DEBUG

    logging.basicConfig(
        level=loglevel, format="%(levelname)s\t %(asctime)s %(message)s"
    )
    logging.getLogger("apscheduler").setLevel(loglevel)
    logging.getLogger("chardet.charsetprober").setLevel(logging.INFO)
