import psycopg2
import backoff

from iskur.metrics import (
    INSERT_DAILY_REQUEST_TIME,
    INSERT_HOURLY_REQUEST_TIME,
    INSERT_WEARTHER_REQUEST_TIME,
    INSERT_ALERTS_REQUEST_TIME,
)

DB_NAME = "iskur"


class PostgresPersistence:
    # pylint: disable=invalid-name
    def __init__(self, args):
        if not args:
            raise ValueError("No args supplied")
        self._args = args

        self._connection_test()
        self._create_tables()

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_tries=15)
    def _connection_test(self) -> None:
        """ Test the connection to the database with given parameters. """
        with psycopg2.connect(
            host=self._args.pg_host,
            user=self._args.pg_user,
            password=self._args.pg_pw,
            dbname=DB_NAME,
        ) as db:
            with db.cursor() as cursor:
                cursor.execute("SELECT 1")

    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_tries=15)
    def _create_tables(self) -> None:
        """ Creates the database tables. """
        with psycopg2.connect(
            host=self._args.pg_host,
            user=self._args.pg_user,
            password=self._args.pg_pw,
            dbname=DB_NAME,
        ) as db:
            with db.cursor() as cursor:
                with open("postgres/01-init.sql", "r") as sql_file:
                    cursor.execute(sql_file.read())
            db.commit()

    @INSERT_HOURLY_REQUEST_TIME.time()
    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_tries=15)
    def persist_hourly(self, records) -> None:
        """ Persist a list of hourly weather forecasts. """
        with psycopg2.connect(
            host=self._args.pg_host,
            user=self._args.pg_user,
            password=self._args.pg_pw,
            dbname=DB_NAME,
        ) as db:
            with db.cursor() as cursor:
                for entry in records:
                    sql = "INSERT INTO hourly (id, city, timestamp, summary, temp, temp_apparent, precip_intensity, precip_prob, precip_type, wind_speed, wind_gust, uv_index, ozone, cloudiness, humidity, visibility, pressure, icon) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (id, city) DO UPDATE SET summary = %s, temp = %s, temp_apparent = %s, precip_intensity = %s, precip_prob = %s, precip_type = %s, wind_speed = %s, wind_gust = %s, uv_index = %s, ozone = %s, cloudiness = %s, humidity = %s, visibility = %s, pressure = %s, icon = %s"

                    cursor.execute(
                        sql,
                        (
                            entry.timestamp.int_timestamp,
                            entry.city,
                            entry.timestamp,
                            entry.summary,
                            entry.temp,
                            entry.temp_apparent,
                            entry.precip_intensity,
                            entry.precip_prob,
                            entry.precip_type,
                            entry.wind_speed,
                            entry.wind_gust,
                            entry.uv_index,
                            entry.ozone,
                            entry.cloudiness,
                            entry.humidity,
                            entry.visibility,
                            entry.pressure,
                            entry.icon,
                            entry.summary,
                            entry.temp,
                            entry.temp_apparent,
                            entry.precip_intensity,
                            entry.precip_prob,
                            entry.precip_type,
                            entry.wind_speed,
                            entry.wind_gust,
                            entry.uv_index,
                            entry.ozone,
                            entry.cloudiness,
                            entry.humidity,
                            entry.visibility,
                            entry.pressure,
                            entry.icon,
                        ),
                    )
                db.commit()

    @INSERT_DAILY_REQUEST_TIME.time()
    @backoff.on_exception(backoff.expo, psycopg2.OperationalError, max_tries=15)
    def persist_daily(self, records) -> None:
        """ Persists a list of daily weather forecast to the database. """
        with psycopg2.connect(
            host=self._args.pg_host,
            user=self._args.pg_user,
            password=self._args.pg_pw,
            dbname=DB_NAME,
        ) as db:
            with db.cursor() as cursor:
                for entry in records:
                    sql = (
                        "INSERT INTO daily (id, city, timestamp, summary, sunrise, sunset, moonphase, temp_high, "
                        "temp_low, temp_app_high, temp_app_low, precip_intensity, "
                        "precip_prob, precip_type, wind_speed, wind_gust, "
                        "uv_index, ozone, cloudiness, humidity, visibility, pressure, icon) "
                        "VALUES(%s,%s,%s,%s,%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                        "%s, %s) ON CONFLICT (id, city) DO UPDATE SET summary = %s, sunrise = %s, sunset = %s, moonphase = %s, temp_high = %s, "
                        "temp_low = %s, temp_app_high = %s, temp_app_low = %s, precip_intensity = %s, "
                        "precip_prob = %s, precip_type = %s, wind_speed = %s, wind_gust = %s, "
                        "uv_index = %s, ozone = %s, cloudiness = %s, humidity = %s, visibility = %s, pressure = %s, icon = %s"
                    )

                    cursor.execute(
                        sql,
                        (
                            entry.timestamp.int_timestamp,
                            entry.city,
                            entry.timestamp,
                            entry.summary,
                            entry.sunrise,
                            entry.sunset,
                            entry.moonphase,
                            entry.temp_high,
                            entry.temp_low,
                            entry.temp_app_high,
                            entry.temp_app_low,
                            entry.precip_intensity,
                            entry.precip_prob,
                            entry.precip_type,
                            entry.wind_speed,
                            entry.wind_gust,
                            entry.uv_index,
                            entry.ozone,
                            entry.cloudiness,
                            entry.humidity,
                            entry.visibility,
                            entry.pressure,
                            entry.icon,
                            entry.summary,
                            entry.sunrise,
                            entry.sunset,
                            entry.moonphase,
                            entry.temp_high,
                            entry.temp_low,
                            entry.temp_app_high,
                            entry.temp_app_low,
                            entry.precip_intensity,
                            entry.precip_prob,
                            entry.precip_type,
                            entry.wind_speed,
                            entry.wind_gust,
                            entry.uv_index,
                            entry.ozone,
                            entry.cloudiness,
                            entry.humidity,
                            entry.visibility,
                            entry.pressure,
                            entry.icon,
                        ),
                    )
                db.commit()

    @INSERT_WEARTHER_REQUEST_TIME.time()
    @backoff.on_exception(backoff.expo, psycopg2.DatabaseError, max_tries=15)
    def persist_segments(self, segments) -> None:
        """ Persists segments that help with choosing what to wear to the database. """
        with psycopg2.connect(
            host=self._args.pg_host,
            user=self._args.pg_user,
            password=self._args.pg_pw,
            dbname=DB_NAME,
        ) as db:
            with db.cursor() as cursor:
                for entry in segments:
                    sql = "INSERT INTO wearther (id, city, int_start, int_end, outfit, accessories, temp_max, temp_min, precip_max, precip_min, wind_max, wind_min, uv_max, uv_min) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (id, city) DO UPDATE SET int_start = %s, int_end = %s, outfit = %s, accessories = %s, temp_max = %s, temp_min = %s, precip_max = %s, precip_min = %s, wind_max = %s, wind_min = %s, uv_max = %s, uv_min = %s"

                    cursor.execute(
                        sql,
                        (
                            entry.start.int_timestamp,
                            entry.city,
                            entry.start,
                            entry.end,
                            entry.outfit,
                            entry.accessories,
                            entry.temp_max,
                            entry.temp_min,
                            entry.precip_max,
                            entry.precip_min,
                            entry.wind_max,
                            entry.wind_min,
                            entry.uv_max,
                            entry.uv_min,
                            entry.start,
                            entry.end,
                            entry.outfit,
                            entry.accessories,
                            entry.temp_max,
                            entry.temp_min,
                            entry.precip_max,
                            entry.precip_min,
                            entry.wind_max,
                            entry.wind_min,
                            entry.uv_max,
                            entry.uv_min,
                        ),
                    )
                db.commit()

    @INSERT_ALERTS_REQUEST_TIME.time()
    @backoff.on_exception(backoff.expo, psycopg2.DatabaseError, max_tries=15)
    def persist_alerts(self, alerts) -> None:
        """ Persists alerts to the database. """
        with psycopg2.connect(
            host=self._args.pg_host,
            user=self._args.pg_user,
            password=self._args.pg_pw,
            dbname=DB_NAME,
        ) as db:
            with db.cursor() as cursor:
                for entry in alerts:
                    sql = "INSERT INTO alerts (id, city, starts, expires, title, description) VALUES(%s, %s, %s, %s, %s, %s) ON CONFLICT (id, city) DO UPDATE SET starts = %s, expires = %s, title = %s, description = %s WHERE alerts.id = %s, alerts.city = %s"

                    cursor.execute(
                        sql,
                        (
                            entry.starts.int_timestamp,
                            entry.city,
                            entry.starts,
                            entry.expires,
                            entry.title,
                            entry.description,
                            entry.starts,
                            entry.expires,
                            entry.title,
                            entry.description,
                            entry.starts.int_timestamp,
                            entry.city,
                        ),
                    )
                db.commit()
