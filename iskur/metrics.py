from prometheus_client import Counter, Gauge, Summary

PROM_PROCESSED_RECORDS = Counter(
    "iskur_processed_records_count", "Records processed", ["type"]
)
PROM_ERRORS_PROCESSING = Counter(
    "iskur_processed_records_errors_count", "Records processed", ["type"]
)
PROM_API_CALLS = Counter("iskur_api_calls_count", "Records processed")
PROM_API_CALLS_ERRORS = Counter(
    "iskur_api_calls_count_errors_count", "Records processed"
)
PROM_API_CALL_TIMESTAMP = Gauge(
    "iskur_api_calls_timestamp_seconds", "Timestamp of last api call", ["outcome"]
)
INSERT_HOURLY_REQUEST_TIME = Summary(
    "iskur_db_inserts_hourly_seconds", "Time needed to insert hourly records"
)
INSERT_DAILY_REQUEST_TIME = Summary(
    "iskur_db_inserts_daily_seconds", "Time needed to insert daily records"
)
INSERT_WEARTHER_REQUEST_TIME = Summary(
    "iskur_db_inserts_wearther_seconds", "Time needed to insert daily records"
)
INSERT_ALERTS_REQUEST_TIME = Summary(
    "iskur_db_inserts_alerts_seconds", "Time needed to insert daily records"
)
PROM_DATABASE_ERRORS = Counter(
    "iskur_processed_db_errors_total",
    "Errors while saving records to the database",
    ["type"],
)
