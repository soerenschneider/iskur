import math


def translate_moonphase(phase: float) -> str:
    """ Translates the moonphase from a float to a string representation. """
    if math.isclose(phase, 0):
        return "New Moon"

    if math.isclose(phase, 0.25):
        return "First Quarter"

    if math.isclose(phase, 0.5):
        return "Full Moon"

    if math.isclose(phase, 0.75):
        return "Last Quarter"

    if 0 < phase < 0.25:
        return "Waxing Crescent"

    if 0.25 < phase < 0.5:
        return "Waxing Gibbous"

    if 0.5 < phase < 0.75:
        return "Waning Gibbous"

    if 0.75 < phase < 1:
        return "Waning Crescent"

    return "Unknown"
