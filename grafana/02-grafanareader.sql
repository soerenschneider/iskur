DO $$DECLARE count int;
BEGIN
SELECT count(*) INTO count FROM pg_roles WHERE rolname = 'grafanareader';
IF count > 0 THEN
    EXECUTE 'REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM grafanareader';
    EXECUTE 'REVOKE USAGE ON SCHEMA public FROM grafanareader';
END IF;
END$$;

DROP USER IF EXISTS grafanareader;
CREATE USER grafanareader WITH PASSWORD 'pass';
GRANT USAGE ON SCHEMA public TO grafanareader;
ALTER DEFAULT PRIVILEGES IN SCHEMA public
GRANT SELECT ON TABLES TO grafanareader;
