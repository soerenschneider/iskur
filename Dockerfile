FROM python:3.8-slim

RUN useradd toor

COPY requirements.txt /opt/iskur/requirements.txt
RUN pip install --no-cache-dir -r /opt/iskur/requirements.txt

COPY postgres /opt/iskur/postgres
COPY iskur/ /opt/iskur/iskur/
WORKDIR /opt/iskur

USER toor
ENV PYTHONPATH $PYTHONPATH:/opt/
CMD ["python3", "/opt/iskur/iskur"]
